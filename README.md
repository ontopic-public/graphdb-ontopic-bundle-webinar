# GraphDB Virtualization enabled by Ontopic Studio Deployment Guide
GraphDB Virtualization enabled by Ontopic Studio is a solution that includes Ontopic Studio and GraphDB.
This repository provides all the necessary files and instructions for deploying this bundle using Docker.

It contains a license valid for 2 months, until Feb 7th 2024.

## Prerequisites
Before deploying the bundle, make sure you have the following software installed on your system:

 - Docker: https://www.docker.com/
 - Docker Compose: https://docs.docker.com/compose/

## Deployment
To deploy the GraphDB-Ontopic bundle, follow these steps:

`docker-compose up -d`

This command will download the necessary Docker images, create containers, and start the services defined in the `docker-compose.yml` file.

## Accessing Ontopic Studio
Once the deployment is complete, you can access Ontopic Studio through your web browser 
by visiting the following URL: http://ontopic.127.0.0.1.nip.io:8081 

The default username and password for logging into Ontopic Studio are both `ontotext`. 
After logging in, you can start exploring the features and capabilities of the GraphDB-Ontopic Bundle.

## Accessing GraphDB
GraphDB is available at http://ontopic.127.0.0.1.nip.io:7200 . No authentication is needed.
